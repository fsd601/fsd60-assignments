StudentLoginPage.html
---------------------------
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Login</title>
</head>
<body>

<h1>Student Login Form</h1>

<form action="LoginServlet" method = "post">
	<table>
		<tr>
			<td>Enter Email-Id</td>
			<td><input type="text" name="emailId" /></td>
		</tr>
		<tr>
			<td>Enter Password</td>
			<td><input type="password" name="password" /></td>
		</tr>
		
		<tr>
	<td></td>
	<td><button>Login</button> New User? <a href='StudentRegister.html'>SingUp</a></td>
</tr>
		
	</table>	
</form>

</body>
</html>


================================================================================================================================
StudentRegister.html

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Student Registration</title>
</head>
<body>

<h1 align="center">Student Registration</h1>

<form action="RegisterServlet" method="post">
	<table align="center">
		<tr>
			<td>Enter StudentId</td>
			<td><input type="text" name="studentId" required /></td>
		</tr>
		<tr>
			<td>Enter StudentName</td>
			<td><input type="text" name="StudentName" /></td>
		</tr>
		<tr>
			<td>Course</td>
			<td><input type="text" name="course" /></td>
		</tr>
		<tr>
			<td>Select Gender</td>
			<td>
				<select name="gender">
					<option value="" selected>Select Gender</option>
					<option value="Male"   > Male   </option>
					<option value="Female" > Female </option>
					<option value="Others" > Others </option>
				</select>
			</td>
		</tr>
		<tr>
			<td>Mobile Number</td>
			<td><input type="text" name="mobile" /></td>
		</tr>
		<tr>
			<td>Enter Email-Id</td>
			<td><input type="text" name="emailId" required /></td>
		</tr>
		<tr>
			<td>Enter Password</td>
			<td><input type="password" name="password"  required/></td>
		</tr>
		
		<tr>
	<td></td>
	<td><button type = "submit">Register</button> Already User? <a href='StudentLoginPage.html'>SignIn</a></td>
</tr>
	</table>	
</form>

</body>
</html>

============================================================================================================================
RegisterServlet


package com.web;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet("/RegisterServlet")
public class RegisterServlet extends HttpServlet {

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
			
		int studentId = Integer.parseInt(request.getParameter("studentId"));
		String studentName = request.getParameter("StudentName");
		String course = request.getParameter("course");
		String gender = request.getParameter("gender");
		String mobile = request.getParameter("mobile");
		String emailId = request.getParameter("emailId");
		String password = request.getParameter("password");
			
		out.print("<html>");
		out.print("<body bgcolor='lightyellow' text='green'>");
		out.print("<center>");
		out.print("<h1>Student Details</h1>");
		out.print("<h3>StudentId   : " + studentId + "</h3>");
		out.print("<h3>StudentName : " + studentName + "</h3>");
		out.print("<h3>Course  : " + course + "</h3>");
		out.print("<h3>Gender  : " + gender + "</h3>");
		out.print("<h3>Mobile  : " + mobile + "</h3>");
		out.print("<h3>Email-Id: " + emailId + "</h3>");
		out.print("<h3>Password: " + password + "</h3>");
		out.print("<center></body></html>");
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		doGet(request, response);
	}

}

===================================================================================================================================
LoginServlet


package com.web;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet("/LoginServlet")
public class LoginServlet extends HttpServlet {

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
			
		String emailId = request.getParameter("emailId");
		String password = request.getParameter("password");
			
		out.print("<html>");
		out.print("<body bgcolor='lightyellow' text='blue'>");
		out.print("<center>");
			
		if (emailId.equalsIgnoreCase("student@gmail.com") && password.equals("1234")) {		
				
			//Calling HRHomePage (Servlet) 
			RequestDispatcher requestDispatcher = request.getRequestDispatcher("StudentHomePage");
			requestDispatcher.forward(request, response);
				
		} else {			
			out.print("<h1 style='color:red'>Invalid Credentials</h1>");
				
			//Calling Login.html 
			RequestDispatcher requestDispatcher = request.getRequestDispatcher("StudentLoginPage.html");
			requestDispatcher.include(request, response);
		}
			
		out.print("<center></body></html>");
	}


	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}

==================================================================================================================
StudentHomePage

package com.web;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class StudentHomePage
 */
@WebServlet("/StudentHomePage")
public class StudentHomePage extends HttpServlet {
	

protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
	response.setContentType("text/html");
	PrintWriter out = response.getWriter();
		
	out.print("<html>");
	out.print("<body bgcolor='lightyellow'>");
	out.print("<center>");
	out.print("<a href='StudentHomePage'>Home</a> &nbsp;");
	out.print("<a href='StudentLoginPage.html'>Logout</a>");
	out.print("<h1 style='color:green'>Welcome to Student Portal</h1>");	
	out.print("<center></body></html>");
		
}


	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
